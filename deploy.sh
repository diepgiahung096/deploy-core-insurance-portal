usertoken=hung.diep:ZthTjV6sz2s1YnY--mPZ
export DEPLOY_HOME="$(pwd)"

echo -e "\t ---->>>> pull project <<<<----\n"
git clone https://$usertoken@gitlab.alpaca.vn/libra/core-insurance/core-insurance-portal.git
echo -e "\n\t ---->>>> pull done <<<<----\n"

echo -e "\t\n ---->>>> Run init-share.sh <<<<----\n"
cd core-insurance-portal
while IFS= read -r line; 
do
    case "$line" in \#*) continue ;; esac
    result="git clone -q https://$usertoken@"${line#*//}
    $result
done <init-share.sh
echo -e "\t ---->>>> Run done <<<<----\n"

echo -e "\t\n ---->>>> yarn install & build <<<<----\n"
yarn install && yarn build
echo -e "\t\n ---->>>> install & build done <<<<----\n"

echo -e "\t\n ---->>>> start deploy <<<<----\n"
mkdir deploy && cp -r -v .next node_modules public package.json $DEPLOY_HOME/deploy/
cd .. && rm -rf core-insurance-portal
cd deploy && yarn start




